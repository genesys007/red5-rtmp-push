package exam;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import net.coobird.thumbnailator.Thumbnails;

public class ImageScale {
	public static void main(String[] args) throws Exception {
		File file = new File("F:/images/1.jpg");
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		InputStream in = new FileInputStream(file);
		Thumbnails.of(in).scale(0.1f).toOutputStream(outputStream);
		byte[] buf = outputStream.toByteArray();
		System.out.println(buf);
	}
}
