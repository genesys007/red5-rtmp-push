var MapHelper = function () {
    var map = null;
    var config={
        center:[13723777.719260542, 4067677.1410778337],
        target: 'map',
        zoomSet:16,
        currentZoom:14,
        maxZoom:19,
        minZoom:14
    };
    var featuresOverlay = null,highlight = null,
        draw = null,
        drawLayerArr = [],
        featureOverlay, vectorLayer,deptVectorLayer,
        gridAreaLayer = new alagis.layer.Vector({
            zIndex: 0
        }),
        pointLayer = new alagis.layer.Vector({
            zIndex: 0
        });


    //初始化地图
    function initMap(_config) {
        config = $.extend(config, _config);
        if (map === null) {
            map = new alagis.Map({
                layers: [
                    ALAGIS_MAP_CONFIG.AlaLayerCustomTile
                ],
                target: config.target,
                view: new alagis.View({
                    center: config.center,
                    zoom: config.currentZoom,
                    maxZoom: config.maxZoom,
                    minZoom: config.minZoom
                })
            });
            alagis.util.hideAttribution();
        }
        map.on('click', function (evt) {
            var _spotIdArr = [];
            map.forEachFeatureAtPixel(evt.pixel, function (feature) {
                if(feature.getProperties().type === "spot" && $.inArray(feature.getProperties()["id"],_spotIdArr) === -1){
                    _spotIdArr.push(feature.getProperties()["id"]);
                }
            });
            if(_spotIdArr.length !== 0){
                window.tableReload(_spotIdArr);
            }
        });


    }
    //清除图层
    function clear() {
        if (gridAreaLayer !== null) {
            if (gridAreaLayer.getSource() !== null) {
                gridAreaLayer.getSource().clear(true);
            }
            map.removeLayer(gridAreaLayer);
        }
        //清除绘画图层
        if (drawLayerArr != null) {
            for (var i = 0; i < drawLayerArr.length; i++) {
                map.removeLayer(drawLayerArr[i]);
                drawLayerArr[i] = null;
            }
        }
        if (draw != null) {
            map.removeInteraction(draw);
        }

        removeHostArea();
    }

    //判断地图是否含有图层
    function isHaveLayer(layer) {
        var st = false;
        var layers = map.getLayers(); //debugger;
        for (var i = 0; i < layers.getArray().length; i++) {
            if (layers.getArray()[i] == layer) {
                st = true;
                break;
            }
        }
        return st;
    }





    function initDeleteHotArea(){
        var geojsonObject = {
            "type": "FeatureCollection",
            "features": []
        };
        $.ajax({
            url:  '/map/hotArea/state1',
            method: 'GET',
            success: function (response) {
                if (response.code == 200) {
                    $.each(response.data, function (index, dda) {
                        var val = response.data[index];
                        var geometry = new alagis.format.GeoJSON().writeGeometryObject(new alagis.format.WKT().readGeometry(val.buildCroodsGeometry));

                        var features = {
                            type: "Feature",
                            id: val.id,
                            properties: {type: 'spot', id: val.id, name: val.buildName, address: val.buildAddress},
                            geometry: geometry
                        };

                        geojsonObject.features.push(features);
                    });
                    if (vectorLayer == null) {
                        vectorLayer = new alagis.layer.Vector({zIndex: 1});
                        vectorLayer.setStyle(createHasOwnSpotStyle());
                    } else {
                        vectorLayer.getSource().clear(true);
                        map.removeLayer(vectorLayer);
                    }
                    vectorLayer.setSource(new alagis.source.Vector({
                        features: (new alagis.format.GeoJSON()).readFeatures(geojsonObject)
                    }));
                    vectorLayer.getSource().forEachFeature(function(sfeat){
                        sfeat.setStyle(createHasOwnSpotStyle(sfeat.get('name')));
                    });
                    map.addLayer(vectorLayer);
                }
            },
            failure: function () {
            }
        });
    }


    function removeHostArea(){
        if (deptVectorLayer != null) {
            deptVectorLayer.getSource().clear(true);
            map.removeLayer(deptVectorLayer);
            deptVectorLayer = null;
        }
        if (vectorLayer != null) {
            vectorLayer.getSource().clear(true);
            map.removeLayer(vectorLayer);
            vectorLayer = null;
        }

        if (featuresOverlay != null) {
            featuresOverlay.getSource().clear(true);
            map.removeLayer(featureOverlay);
            featuresOverlay = null;
        }
    }

    /**
     * 创建已有热区默认样式
     * @returns {alagis.style.Style}
     */
    function createHasOwnSpotStyle(text) {
        return new alagis.style.Style({
            fill: new alagis.style.Fill({
                color: 'rgba(255,255,255,0.3)'
            }),
            stroke: new alagis.style.Stroke({
                color: '#2ABDE2',
                width: 1
            }),
            text: new alagis.style.Text({
                text:text||"",
                font: '12px Calibri,sans-serif',
                fill: new alagis.style.Fill({
                    color: '#000'
                }),
                stroke: new alagis.style.Stroke({
                    color: '#fff',
                    width: 1
                })
            })
        });
    }

    /**
     * 创建鼠标移动到热区样式
     * @returns {alagis.style.Style}
     */
    function createOverLayStyle(text) {
        return new alagis.style.Style({
            stroke: new alagis.style.Stroke({
                color: '#ffd800',
                width: 4
            }),
            fill: new alagis.style.Fill({
                color: 'rgba(255,255,255,0.6)'
            }),
            text: new alagis.style.Text({
                text:text||"",
                font: '12px Calibri,sans-serif',
                fill: new alagis.style.Fill({
                    color: '#000'
                }),
                stroke: new alagis.style.Stroke({
                    color: '#fff',
                    width: 3
                })
            })
        });
    }




    return {
        initMap: function (_config) {
            initMap(_config);
        },
        clear: function () {
            clear();
        },
        initDeleteHotArea:function(){
            initDeleteHotArea();
        },
        removeHostArea:function(){
            removeHostArea();
        }
    }
}();