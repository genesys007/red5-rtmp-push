MapHelper.initMap({
    zoom: 14,
    maxZoom: 19,
    minZoom: 14
});

setTimeout(function(){
    MapHelper.initDeleteHotArea();
},3000);


$('#buildTable').bootstrapTable(
    {
        method : 'get', // 服务器数据的请求方式 get or post
        url :  "/map/hotArea/list", // 服务器数据的加载地址
        iconSize: 'outline',
        striped: true, // 设置为true会有隔行变色效果
        dataType: "json", // 服务器返回的数据类型
        pagination: false, // 设置为true会在底部显示分页条
        singleSelect: false, // 设置为true将禁止多选
        uniqueId:"id",
        queryParamsType: "",
        queryParams : function(params) {
            return {
                buildIds:[]
            };
        },
        // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
        // queryParamsType = 'limit' ,返回参数必须包含
        // limit, offset, search, sort, order 否则, 需要包含:
        // pageSize, pageNumber, searchText, sortName,
        // sortOrder.
        // 返回false将会终止请求
        responseHandler : function(res){
            return res.data;
        },
        columns : [
            {
                field: 'no',
                title: '序号',
                width:40,
                formatter: function (value, row, index) {
                    //获取每页显示的数量
                    var pageSize=$('#buildTable').bootstrapTable('getOptions').pageSize;
                    //获取当前是第几页
                    var pageNumber=$('#buildTable').bootstrapTable('getOptions').pageNumber;
                    //返回序号，注意index是从0开始的，所以要加上1
                    return pageSize * (pageNumber - 1) + index + 1;
                }
            },
            {
                field : 'buildName',
                width:100,
                title : '热区名称'
            },
            {
                title : '操作',
                field : 'id',
                width:30,
                align : 'center',
                formatter : function(value, row, index) {
                    var  btnContent = '';
                    btnContent += '<button type="button" class="btn btn-warning btn-xs" onclick="recoverHotArea(\''
                        + row.id+ '\',\''+row.buildName+'\')">恢复热区</button> ';
                    btnContent += '<button type="button" class="btn btn-danger btn-xs" onclick="deleteHotArea(\''
                        + row.id+ '\',\''+row.buildName+'\')">删除热区</button>  ';
                    return btnContent;
                }
            } ]
    });

function recoverHotArea(buildId,buildName){
    layer.confirm("确定要恢复热区【"+buildName+"】吗",function(){
        $.ajax({
            url:"/map/hotArea/recover",
            data:{buildId:buildId},
            type:"post",
            success:function(res){
                if(res.code == 200){
                    reloadHotArea();
                    $('#buildTable').bootstrapTable('removeByUniqueId', buildId);
                    layer.alert("恢复成功！");
                }
            }
        })
    });
}
function deleteHotArea(buildId,buildName){
    layer.confirm("确定要彻底删除热区【"+buildName+"】吗",function(){
        $.ajax({
            url:"/map/hotArea/delete",
            data:{buildId:buildId,real:true},
            type:"post",
            success:function(res){
                if(res.code == 200){
                    reloadHotArea();
                    $('#buildTable').bootstrapTable('removeByUniqueId', buildId);
                    layer.alert("删除成功！");
                }
            }
        })
    });
}
function tableReload(buildIds){
    $('#buildTable').bootstrapTable('refresh',{query: {buildIds:buildIds}});

}

function reloadHotArea(){
    MapHelper.clear();
    MapHelper.initDeleteHotArea();
}

