var config = {
    zoomSet:15,
    currentZoom:19,
    maxZoom:21,
    minZoom:1
};

var wgLayerPoint = null; 
var c = ol.proj.transform([118.95632939373546, 42.294237816899766], 'EPSG:4326', 'EPSG:3857');

config.center = c;
 
MapHelper.initMap(config);

setTimeout(function(){
    MapHelper.initHotArea();
},3000);

$('#buildTable').bootstrapTable(
        {
            method : 'get', // 服务器数据的请求方式 get or post
            url :  "/map/hotArea/list", // 服务器数据的加载地址
            iconSize: 'outline',
            striped: true, // 设置为true会有隔行变色效果
            dataType: "json", // 服务器返回的数据类型
            pagination: false, // 设置为true会在底部显示分页条
            singleSelect: false, // 设置为true将禁止多选
            uniqueId:"id",
            queryParamsType: "",
            queryParams : function(params) {
                return {
                    buildIds:[]
                };
            },
            // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
            // queryParamsType = 'limit' ,返回参数必须包含
            // limit, offset, search, sort, order 否则, 需要包含:
            // pageSize, pageNumber, searchText, sortName,
            // sortOrder.
            // 返回false将会终止请求
            responseHandler : function(res){
                return res.data;
            },
            columns : [
                {
                    field: 'no',
                    title: '序号',
                    width:40,
                    formatter: function (value, row, index) {
                        //获取每页显示的数量
                        var pageSize=$('#buildTable').bootstrapTable('getOptions').pageSize;
                        //获取当前是第几页
                        var pageNumber=$('#buildTable').bootstrapTable('getOptions').pageNumber;
                        //返回序号，注意index是从0开始的，所以要加上1
                        return pageSize * (pageNumber - 1) + index + 1;
                    }
                },
                {
                    field : 'buildName',
                    width:100,
                    title : '热区名称',
                    formatter : function(value, row, index) {
                        return '<a href="javascript:editBuildInfoById(\''
                            + row.id+ '\',\''+row.buildName+'\',\''+index+'\')" style="border-bottom: dashed 1px #0088cc;">'+value+'</a>';
                    }
                },
                {
                    title : '操作',
                    field : 'id',
                    width:30,
                    align : 'center',
                    formatter : function(value, row, index) {
                        var  btnContent = '';
                        btnContent += '<button type="button" class="btn btn-primary btn-xs" onclick="drawHotArea(\''
                            + row.id+ '\',\''+row.buildName+'\')">重绘热区</button> ';
                        btnContent += '<button type="button" class="btn btn-danger btn-xs" onclick="deleteHotArea(\''
                            + row.id+ '\')">删除热区</button>  ';
                        return btnContent;
                    }
                } ]
        });

$("#create").click(function(){
    clearForm();
    drawHotAreaCZ(function(){
        layer.open({
            type: 1,
            title: "新增摄像头",
            area: ['420px', '200px'], //宽高
            content: $("#formDiv").html(),
            btn: ['提交','重绘',  '取消'],
            yes: function(index, layero){
                var nname = layero.find('#name').val();
                if(!nname){
                    layer.msg("请输入热区名称！");
                    return false;
                }
                $("#name").val(nname);
                $.ajax({
                    type : "POST",
                    url : "/map/hotArea/save",
                    data : $('#infoForm').serialize(),// 你的formid
                    success : function(data) {
                        if (data.code == 200) {
                            clearForm();
                            reloadHotArea();
                            layer.close(index);
                            layer.alert("操作成功");
                        }else{
                            layer.alert(data.msg);
                        }

                    }
                });
            }
            ,btn2: function(index, layero){
                layer.close(index);
                
                $("#create").click();
            }
            ,btn3: function(index, layero){
                layer.close(index);
                clearForm();
                reloadHotArea();
            },cancel: function(){
                clearForm();
                reloadHotArea();
            }
        });
    });
});

$("#cm").click(function(){
    drawHotAreaCZ();
});
$("#done").click(function(){
    if(!$("#area").val()){
        layer.alert("尚未绘制热区");
        return false;
    }
    $.ajax({
        type : "POST",
        url : "/map/hotArea/save",
        data : $('#infoForm').serialize(),// 你的formid
        success : function(data) {
            if (data.code == 200) {
                clearHiddenValue();
                reloadHotArea();
                layer.alert("重绘成功");
            }else{
                layer.alert(data.msg);
            }
            $("#cmdiv").hide();
            $("#newdiv").show();
        }
    });

});

$("#cancel").click(function(){
    $("#cmdiv").hide();
    $("#newdiv").show();
    clearForm();
    reloadHotArea();
});


function editBuildInfoById(buildId,buildName,_index){
    clearForm();
    $("#id").val(buildId);
    $("#name").val(buildName);
    $("#name").attr("value",buildName);
    layer.open({
        type: 1,
        title: "编辑热区",
        area: ['420px', '200px'], //宽高
        content: $("#formDiv").html(),
        btn: ['提交',  '取消'],
        yes: function(index, layero){
            var nname = layero.find('#name').val();
            if(!nname){
                layer.msg("请输入热区名称！");
                return false;
            }
            $("#name").val(nname);
            $.ajax({
                type : "POST",
                url : "/map/hotArea/update",
                data : $('#infoForm').serialize(),// 你的formid
                success : function(res) {
                    if (res.code == 200) {
                        clearHiddenValue();
                        reloadHotArea();
                        layer.close(index);
                        $('#buildTable').bootstrapTable('updateRow',{index:_index,row:res.data});
                        layer.alert("修改成功");
                    }else{
                        layer.alert(res.msg);
                    }
                }
            });

        }
        ,btn2: function(index, layero){
            layer.close(index);
        }
    });
}

function drawHotArea(buildId,buildName){
    $("#cmdiv").show();
    $("#newdiv").hide();
    $("#id").val(buildId);
    $("#name").val(buildName);
    drawHotAreaCZ();

}


function deleteHotArea(buildId){
    var curData = $('#buildTable').bootstrapTable('getRowByUniqueId',buildId);
    layer.confirm("确定要删除热区【"+curData.buildName+"】吗",function(){
        $.ajax({
            url:"/map/hotArea/delete",
            data:{buildId:buildId},
            type:"post",
            success:function(res){
                if(res.code == 200){
                    reloadHotArea();
                    $('#buildTable').bootstrapTable('removeByUniqueId', buildId);
                    layer.alert("删除成功！");
                }
            }
        })
    });
}
function tableReload(buildIds){
    $('#buildTable').bootstrapTable('refresh',{query: {buildIds:buildIds}});

}

function reloadHotArea(){
    MapHelper.clear();
    MapHelper.initHotArea();
}

function drawHotAreaCZ(callback){
    clearHiddenValue(); 
    /*MapHelper.drawIcon({
        strokeWidth:4,
        type: 'point''Polygon', callback: function (coordinates) {
            $("#area").val(new ol.format.WKT().writeGeometry(coordinates.feature.getGeometry()));
            var xy = coordinates.feature.getGeometry().getInteriorPoint().getCoordinates();
            $("#lng").val(xy[0]);
            $("#lat").val(xy[1]);
            if($.isFunction(callback)){
                callback();
            }
        }
    })*/
     
    var ttpye = "Point";
    MapHelper.clear();
    console.log(wgLayerPoint);
    if (wgLayerPoint !== null) {
        if (wgLayerPoint.getSource() !== null) {
        	wgLayerPoint.getSource().clear(true);
        }
       
    }
    
    MapHelper.newOndraw({ 
        type: ttpye, callback: function (coordinates) { 
            var xy = "";
            if (ttpye != "Polygon") {
                xy = coordinates.feature.getGeometry().getFirstCoordinate()
            } else {
                xy = coordinates.feature.getGeometry().getInteriorPoint().getCoordinates();
            }
            var pointDatas = [];
            pointDatas.push({ data: { type: 'gridarea',  x: xy[0], y: xy[1]}, name: '标记', offsetX: 0, offsetY: -8, noText: true, style: { icon: '/images/cctv.png' } });
          
            if(wgLayerPoint == null){
                wgLayerPoint = new ol.layer.Vector({
                    zIndex: 0,
                    source: new ol.source.Vector()
                });
            }
            MapHelper.drawIcon({
                layer: wgLayerPoint,
                properties: pointDatas
            })
			//parent.getLocation();
            MapHelper.newUndraw();
            //parent.layer.close(parent.layer.getFrameIndex(window.name));
            //debugger;
            if($.isFunction(callback)){
                callback();
            }
        }
    })
	 
}
function clearForm(){
    $("#id").val("");
    $("#name").val("");
    $("#name").attr("value","");
    clearHiddenValue();
}
function clearHiddenValue(){
    $("#area").val("");
    $("#lng").val("");
    $("#lat").val("");
}
