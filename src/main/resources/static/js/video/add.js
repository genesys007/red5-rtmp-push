var prefix = "/video"
$().ready(function(){ 
	selectedHtml("groupType", "group_type", $("#groupTypeValue").val()); 
	validateRule(); 
});

$.validator.setDefaults({
	submitHandler : function() {
		update();
	}
});

$("#position").click(function(){
	 localStorage.removeItem('lat');
     localStorage.removeItem('lng'); 
     localStorage.setItem('lat',$("#lat").val());
     localStorage.setItem('lng',$("#lng").val());
	var idex = layer.open({
		type : 2,
		title: false, //标题不显示
		closeBtn: 0, //右上角关闭不显示
		maxmin : false,
		shadeClose : false, // 点击遮罩关闭层
		area :  ['100%', '100%'],//['285px','320px'],//
		content : '/video/map',
	});
});

function update() {
	$.ajax({
		cache : true,
		type : "POST",
		url : prefix+"/save",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.location.reload();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
 

function validateRule() { 
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
    	rules : {
    		name : {
    			required : true
            },
            appName : {
    			required : true
            },
            groupType : {
            	required : true
            },  
            fetchUrl : {
            	required : true
            }, 
            pushUrl : {
            	required : true
            },
            sort : {
            	required : true,
                digits : true,
                maxlength : 12 
            },
            videoImg : {
            	required : true
            } 
            
        },
        messages : {
        	name : {
        		required : icon + "请输入标题"
            },
            appName : {
        		required : icon + "请输入英文名称"
            },
            groupType : {
            	required : icon + "选择分组"
            }, 
            fetchUrl : {
            	required : icon + "添加视频源"
            },
            pushUrl : {
            	required : icon + "添加推送地址"
            }, 
            sort : {
            	required : icon + "请输入正整数",
            	digits : icon + "请输入正整数",
            	maxlength : icon + "输入值过大"
            },
            videoImg : {
            	required : icon + "添加视频图片"
            }
        }
    })
}

//字典
var selectedHtml = function(selctId,gValue,hxId){
    $("#"+selctId).empty();
    $.ajax({
        cache : true,
        type : "GET",
        url : "/common/sysDict/listByGroupType",
        data : {"gValue":gValue},
        async : false,
        error : function(request) {
            alertMsg("通信出错!");
        },
        success : function(data) {
            if (data.code == 0) {
                var arr = data.data;
                var option = "<option value=''>-请选择-</option>";
                if(hxId == ""){
                    option = "<option value='' selected='selected'>-请选择-</option>";
                }else{
                    option = "<option value='' >-请选择-</option>";
                }
                $("#"+selctId).append(option);
                $.each(arr,function(index,obj){
                    if(obj.value != null){
                        var htmlRes = "";
                        if(obj.value == hxId){
                            htmlRes = "<option value='"+obj.value+"' selected='selected'>"+obj.name+"</option>";
                        }else{
                            htmlRes = "<option value='"+obj.value+"'>"+obj.name+"</option>";
                        }
                        $("#"+selctId).append(htmlRes);
                    }
                });
            } else {
                alertMsg(data.msg)
            }
        }
    });
};
//压缩方法
function dealImage(base64, w,h, callback) {
    var newImage = new Image();
    var quality = 0.03;    //压缩系数0-1之间
    newImage.src = base64;
    //newImage.setAttribute("crossOrigin", 'Anonymous');	//url为外域时需要
    var imgWidth, imgHeight;
    newImage.onload = function () {
        imgWidth = w;
        imgHeight = h;
        var canvas = document.createElement("canvas");
        var ctx = canvas.getContext("2d");
      /*   if (Math.max(imgWidth, imgHeight) > w) {
            if (imgWidth > imgHeight) {
                canvas.width = w;
                canvas.height = w * imgHeight / imgWidth;
            } else {
                canvas.height = w;
                canvas.width = w * imgWidth / imgHeight;
            }
        } else {
            canvas.width = imgWidth;
            canvas.height = imgHeight;
            quality = 0.6;
        } */
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
        var base64 = canvas.toDataURL("image/png", quality); //压缩语句
    /*     // 如想确保图片压缩到自己想要的尺寸,如要求在50-150kb之间，请加以下语句，quality初始值根据情况自定
        while (base64.length / 1024 > 150) {
        	quality -= 0.01;
        	base64 = canvas.toDataURL("image/png", quality);
        }
        // 防止最后一次压缩低于最低尺寸，只要quality递减合理，无需考虑
        while (base64.length / 1024 < 50) {
        	quality += 0.001;
        	base64 = canvas.toDataURL("image/png", quality);
        } */
        callback(base64);//必须通过回调函数返回，否则无法及时拿到该值
    }
}