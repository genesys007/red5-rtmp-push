var prefix = "/sys/role";
$(function() {
    layui.use('laypage', function(){
        var laypage = layui.laypage;
        var pageNo = $("#pageNo").val();
        var pageSize = $("#pageSize").val();
        var total = $("#total").val();
        laypage.render({
            elem: 'pageId',
            count: total,
            curr: pageNo,
            limit: pageSize,
            first:"首页",
            prev:"上一页",
            last:"尾页",
            next:"下一页",
            theme:"#1E9FFF",
            jump: function(obj, first){
                if(first != true){//是否首次进入页面
                    var curr = obj.curr;
                    $("#pageNo").val(curr);
                    $("#form").submit();
                }
            }
        });
    });
});

var search = function () {
    $("#pageNo").val(1);
    $("#pageSize").val(10);
    $("#form").submit();
};
//新增
function add() {
    layui.use('layer', function() {
        var layer = layui.layer;
        layer.open({
            type : 2,
            title : '增加',
            maxmin : true,
            shadeClose : false, // 点击遮罩关闭层
            area : [ '600px', '520px' ],
            content : prefix + '/addRole'
        });
    });
}

function edit(id) {
    layui.use('layer', function() {
        var layer = layui.layer;
        layer.open({
            type : 2,
            title : '编辑',
            maxmin : true,
            shadeClose : false, // 点击遮罩关闭层
            area : [ '800px', '520px' ],
            content : prefix + '/edit/' + id
        });
    });
}
function remove(id) {
    layui.use('layer', function() {
        var layer = layui.layer;
        layer.confirm('确定要删除选中的记录？', {
            btn : [ '确定', '取消' ]
        }, function(index) {
            $.ajax({
                url : prefix + "/remove",
                type : "post",
                data : {
                    'id' : id
                },
                success : function(r) {
                    if (r.code == 0) {
                        layer.close(index);
                        search();
                    } else {
                        layer.msg(r.msg);
                    }
                }
            });
        })
    });
}

function openMenu(roleId) {
    layer.open({
        type: 2,
        title: '选择单位',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['800px', '520px'],
        content:  '/sys/menu/menu_tree/'+roleId
    });
}