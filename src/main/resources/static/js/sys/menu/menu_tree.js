var prefix = "/sys";
$(function () {
    getTreeData();
});

function confirm() {
    var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
    var nodes = treeObj.getCheckedNodes(true);
    var list = new Array();
    for (var i = 0; i < nodes.length; i++) {
        var obj = new Object();
        obj.id = nodes[i].id;
        obj.type = nodes[i].menutype;
        list.push(obj);
    }
    $.ajax({
        type: "GET",
        url: "/sys/menu/saveFeatures",
        data: {"data": JSON.stringify(list), "roleId": $('#roleId').val()},
        success: function (csv) {
            if (csv.code == 0) {
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);
            } else {
                layer.msg(csv.msg);
            }

        }
    })
}

function getTreeData() {
    $.ajax({
        type: "GET",
        url: "/sys/menu/treeList",
        data: {"roleId": $('#roleId').val()},
        success: function (tree) {
            loadTree(tree);
        }
    });
}

function loadTree(tree) {
    var setting = {
        check: {
            enable: true,
            chkboxType :{ "Y" : "ps", "N" : "" }
        },
        data: {
            simpleData: {
                enable: true
            }
        }
    };
    $.fn.zTree.init($("#treeDemo"), setting, tree);
    // $('#jstree').jstree({
    //     'core' : {
    //         'data' : tree
    //     },
    //     'checkbox':{
    //         'keep_selected_style': false,
    //         'three_state':false,
    //         'cascade':'down'
    //     },
    //     "plugins" : [ "checkbox" ]
    // });
    // $('#jstree').jstree().open_all();
}