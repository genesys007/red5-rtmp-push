$().ready(function() {
    validateRule();
});


$.validator.setDefaults({
    submitHandler : function() {
        update();
    }
});
function update() {
    $.ajax({
        cache : true,
        type : "POST",
        url : "/sys/user/resetPwd.do",
        data : $('#signupForm').serialize(),
        async : false,
        error : function(request) {
            parent.layer.alert("Connection error");
        },
        success : function(data) {
            if (data.code == 0) {
                parent.layer.msg("操作成功");
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);
                // window.location.reload(true);
                //加载上一个页面并刷新，强行解决火狐使用window.location.reload(true);不刷新页面
                window.location.replace(document.referrer);
            } else {
                alertMsgTop(data.msg);
            }

        }
    });

}

function validateRule() {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules : {
            oldPassword : {
                required : true
            },
            newPassword : {
                required : true,
                minlength:6
            },
            confirmPassword : {
                required : true,
                minlength:6
            }
        },
        messages : {
            oldPassword : {
                required : icon + "请输入原始名"
            },
            newPassword : {
                required : icon + "请输入新密码",
                minlength: $.validator.format("最少需要输入 {0} 个字符")
            },
            confirmPassword : {
                required : icon + "请输入确认密码",
                minlength: $.validator.format("最少需要输入 {0} 个字符")
            }
        }
    })
}




