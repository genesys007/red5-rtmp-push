/**
 * 
 */
package com.ifast.sys.filter.token;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.lang.annotation.*;

/** 
 * 项目名称：fpbigdata
 * 类名称：CsrfTokenCheck
 * 类描述： 
 * 创建人：ShiQiang
 * 创建时间：2017年8月8日 上午9:35:31
 * 修改人：Administrator 
 * 修改时间：2017年8月8日 上午9:35:31
 * 修改备注： 
 * @version 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
//最高优先级
@Order(Ordered.HIGHEST_PRECEDENCE)
public  @interface CsrfTokenCheck {
  
	
/**
    * 
    * 存放一次性csrf到session
    */
   boolean putValue() default false;

   /**
    * 
    * 存放和session时长一样的csrf
    */
   boolean putLongValue() default false;
   
   /**
    * 
    * ajax存放一次性csrf到session
    */
   boolean putAjax() default false;

   /**
    * 
    * ajax存放和session时长一样的csrf
    */
   boolean putAjaxLongValue() default false;
   	

	
  /**
    * 
    * 是否对一次性csrf进行重复提交验证
    */
   boolean checkValue() default false;

   /**
    * 
    * 是否进行session时长验证
    */
   boolean checkLongValue() default false;
   
   /**
    * 
    * ajax重复提交验证
    */
   boolean checkAjaxValue() default false;

   /**
    * 
    * ajax提交是否进行长时间验证
    */
   boolean checkAjaxLongValue() default false;
   
   
}
