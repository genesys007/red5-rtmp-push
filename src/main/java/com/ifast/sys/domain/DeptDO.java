package com.ifast.sys.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * <pre>
 * 部门管理
 * </pre>
 * <small> 2018年3月23日 | Aron</small>
 */
@TableName("sys_dept")
@Data
public class DeptDO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    // 上级部门ID，一级部门为0
    @TableField(value = "PARENTID")
    private String parentId;
    // 部门名称
    @TableField(value = "NAME")
    private String name;
    // 排序
    @TableField(value = "ORDERNUM")
    private Integer orderNum;
    // 是否删除 -1：已删除 0：正常
    @TableField(value = "DELFLAG")
    private Integer delFlag; 
    //邮编
    private String bmbh; 
    @TableField(exist=false)
    private Integer pageSize = 10;

    @TableField(exist=false)
    private Integer pageNo = 1;
}
