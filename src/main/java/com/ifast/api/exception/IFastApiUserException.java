package com.ifast.api.exception;

import com.ifast.common.exception.IFastException;

/**
 * <pre>
 * API异常基类
 * </pre>
 * 
 * <small> 2018年4月19日 | Aron</small>
 */
public class IFastApiUserException extends IFastException {

    private static final long serialVersionUID = -4891641110275580161L;

    public IFastApiUserException() {
        super();
    }

    public IFastApiUserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public IFastApiUserException(String message, Throwable cause) {
        super(message, cause);
    }

    public IFastApiUserException(String message) {
        super(message);
    }

    public IFastApiUserException(Throwable cause) {
        super(cause);
    }

}
