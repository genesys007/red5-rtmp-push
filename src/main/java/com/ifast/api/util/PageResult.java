package com.ifast.api.util;



import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;

public class PageResult<T> extends Result<T>{
	
	/**
	 * 是否有下一页
	 */
	private boolean hasNext = true;
	
	/**
	 * 当前页数
	 */
	private Long pageNum;
	
	/**
	 * 分页数据
	 */
	private List<Map<String,Object>> listData = Lists.newArrayList();
	
	
	public PageResult(){
		this.code = CODE_SUCCESS;
		this.msg = MSG_SUCCESS;
	}
	
	public PageResult(Page<?> page){
		this.pageNum = page.getCurrent();
        hasNext = page.getTotal() > page.getCurrent();
	}
	
	public boolean isHasNext() {
		return hasNext;
	}

	public PageResult<T> setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
		return this;
	}

	public Long getPageNum() {
		return pageNum;
	}

	public PageResult<T> setPageNum(Long pageNum) {
		this.pageNum = pageNum;
		return this;
	}

	public List<Map<String, Object>> getListData() {
		return listData;
	}

	public void setListData(List<Map<String, Object>> listData) {
		this.listData = listData;
	}
	
	/**
	 * 
	 *@Description: 往listData中添加数据 
	 *@Author: jingWenguang
	 *@Since: 2016年8月17日下午5:14:48
	 *@param map
	 */
	public void add(Map<String,Object> map){
		this.getListData().add(map);
	} 
	
	public static <T> PageResult<T> ok() {
        return new PageResult<T>(CODE_SUCCESS, MSG_SUCCESS);
    }

    public static <T> PageResult<T> fail() {
        return new PageResult<T>(CODE_FAIL, MSG_FAIL);
    }
    
    public static <T> PageResult<T> fail(String msg) {
        return new PageResult<T>(CODE_FAIL, msg);
    }
    
    public static <T> PageResult<T> fail(Integer code,String msg) {
        return new PageResult<T>(code, msg);
    }
    
    public PageResult(Integer code,String msg) {
         this.code = code;
         this.msg = msg;
    }
}
