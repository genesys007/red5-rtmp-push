package com.ifast.api.util;

public enum EnumResultUtil {
	TOKENERR("tokenerr"),
	NORMALERR("normalerr");
	
    private String type;

	EnumResultUtil(String type) {
        this.type = type;
    }

    public String value() {
        return type;
    }

}
