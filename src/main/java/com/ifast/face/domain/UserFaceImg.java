package com.ifast.face.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@TableName("USER_FACE_IMG")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper=false)
public class UserFaceImg extends Model<UserFaceImg>{ 
	/**
	 * 
	 */
	private static final long serialVersionUID = -2249116587535833876L;
	@TableId(type= IdType.AUTO)
    private Long id; 
    private int width;
    private int hight;
    private int state;
    private byte[] img;
	@Override
	protected Serializable pkVal() { 
		return this.id;
	} 
}