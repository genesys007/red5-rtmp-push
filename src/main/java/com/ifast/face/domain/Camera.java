package com.ifast.face.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@TableName("NET_CAMERA")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Camera { 
	@TableId(type= IdType.AUTO)
    private Long id; 
    private String name;  
    private String createTime;
    private String createBy;
    private String fetchUrl;
    private String rtmpUrl;
    private String pushUrl;
    private String appName;
    private Integer status;  //0是未推送，1是推送成功，2推送失败
    private Integer sort;
    private String groupType;
    @TableField(exist = false)
    private String groupName;
    private String videoImg;
    private String lat;
    private String lng;
}