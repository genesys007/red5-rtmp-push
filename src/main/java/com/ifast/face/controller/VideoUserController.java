package com.ifast.face.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ifast.common.annotation.Log;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.service.DictService;
import com.ifast.common.utils.Result;
import com.ifast.face.domain.UserFaceInfo;
import com.ifast.face.service.UserFaceInfoService;
import com.ifast.sys.filter.token.CsrfTokenCheck;

/** 
 * @author shíQíang㊚ 
 * @date 2019年8月16日10:35:19
 */

@Controller
@RequestMapping("/video/user")
public class VideoUserController extends AdminBaseController {
	
	private String preHtml = "video/user/";
	
	@Autowired
    private UserFaceInfoService userFaceInfoService;
	@Autowired
    private DictService sysDictService;
     
    @GetMapping()
    @RequiresPermissions("video:user:list")
    String Config() {
        return preHtml+"list";
    }
     
    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("video:user:list")
    public Result<IPage<UserFaceInfo>> list(UserFaceInfo user) { 
        // 查询列表数据
        IPage<UserFaceInfo> page = userFaceInfoService.page(getPage(UserFaceInfo.class), 
        		new QueryWrapper<UserFaceInfo>()
        		.like(StringUtils.isNotBlank(user.getName()),"name", user.getName())
        		.eq(user.getGroupId() != null, "group_id",user.getGroupId())
        		.orderByDesc("create_time"));
        if(page!=null && page.getRecords() != null){
        	page.getRecords().stream().forEach(u -> {u.setGroupName(sysDictService.getName("group_type", u.getGroupId()+""));});
        } 
        return Result.ok(page);
    }
     
    @GetMapping("/add")
    @RequiresPermissions("video:user:add")
    @CsrfTokenCheck(putValue=true)
    String add() {
        return preHtml+"add";
    }
     
    @GetMapping("/edit/{id}")
    @RequiresPermissions("video:user:edit")
    @CsrfTokenCheck(putValue=true)
    String edit(@PathVariable("id") Long id, Model model) {
    	UserFaceInfo user = userFaceInfoService.getById(id);
        model.addAttribute("info", user);
        return preHtml+"add";
    }

    /**
     * 保存
     */
    @Log("添加")
    @ResponseBody
    @PostMapping("/save")
    @RequiresPermissions("video:user:add")
    @CsrfTokenCheck(checkAjaxValue=true,putAjax=true)
    public Result<?> save(HttpServletRequest request,UserFaceInfo config){ 
        try {
        	return userFaceInfoService.add(request, config.setCreateId(this.getUserId()+""));
		} catch (Exception e) { 
			e.printStackTrace();
		}
        return Result.fail();
    }

    /**
     * 修改
     */
    @Log("更新")
    @ResponseBody
    @RequestMapping("/update")
    @RequiresPermissions("video:user:edit")
    @CsrfTokenCheck(checkAjaxValue=true,putAjax=true)
    public Result<?> update(HttpServletRequest request,UserFaceInfo config) {
    	try {
    		return  userFaceInfoService.update(request, config.setUpdateId(this.getUserId()+""));
  		} catch (Exception e) { 
  			e.printStackTrace();
  		}
        return Result.fail();
    }
    /**
     *  
     */
    @Log("更改状态")
    @PostMapping("/reset")
    @ResponseBody
    @RequiresPermissions("video:user:reset")
    public Result<String> changeStatus(Long id){
    	UserFaceInfo user = userFaceInfoService.getById(id);
    	if(user != null){
    		userFaceInfoService.updateById(user.setChecked((user.getChecked() == null || user.getChecked() == 1 )? 0:1));
    	} 
        return Result.ok();
    }
    /**
     * 删除
     */
    @Log("删除")
    @PostMapping("/remove")
    @ResponseBody
    @RequiresPermissions("video:user:remove")
    public Result<String> remove(Long id) {
        userFaceInfoService.removeById(id);
        return Result.ok();
    }

    /**
     * 删除
     */
    @Log("批量删除")
    @PostMapping("/batchRemove")
    @ResponseBody
    @RequiresPermissions("video:user:batchRemove")
    public Result<String> remove(@RequestParam("ids[]") Long[] ids) {
        userFaceInfoService.removeByIds(Arrays.asList(ids));
        return Result.ok();
    }

}
