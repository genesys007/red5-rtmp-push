package com.ifast.face.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Author: ShiQiang
 * @Date: 2019年5月24日11:31:17
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FaceSearchResult{
	private Long id;
    private String faceId;
    private String name;
    private Integer similarValue;
    private Integer age;
    private Integer gender;
    private String image; 
    private String facePath;
    private String pathType;
}
