package com.ifast.face.service;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.arcsoft.face.FaceFeature;
import com.arcsoft.face.FaceInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ifast.face.domain.UserFaceInfo;
import com.ifast.face.dto.FaceSearchResult;
import com.ifast.face.dto.FaceUserInfo;
import com.ifast.face.dto.ProcessInfo;
import com.ifast.face.util.ImageInfo;

public interface FaceEngineService extends IService<UserFaceInfo> {

    void refresh(Long groupId);
     
    List<FaceInfo> detectFaces(ImageInfo imageInfo);
    
    boolean whetherHasFaces(BufferedImage bufImage);

    List<ProcessInfo> process(ImageInfo imageInfo);

    /**
     * 人脸特征
     * @param imageInfo
     * @return
     */
    List<ProcessInfo> extractFaceFeature(ImageInfo imageInfo) throws InterruptedException;

    /**
     * 人脸比对
     * @param groupId
     * @param faceFeature
     * @return
     */
    List<FaceUserInfo> compareFaceFeature(FaceFeature faceFeatures,Long groupId) throws InterruptedException,ExecutionException;
    /**
     * 关键点
     * @param bufImage
     * @param groupId
     * @return
     * @throws Exception
     */
    public List<FaceSearchResult> findUsersByFaces(BufferedImage bufImage,Long groupId);
    
    public BufferedImage drow(BufferedImage bufImage) throws Exception;
    
    public BufferedImage drowFace(BufferedImage bufImage);
    
    List<FaceInfo> detectFaces(BufferedImage bufImage);
    
    public int faceSimilarScore(BufferedImage src, BufferedImage target);
}
