package com.ifast.face.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ifast.face.domain.UserFaceImg;


public interface UserFaceImgService extends IService<UserFaceImg>{}
