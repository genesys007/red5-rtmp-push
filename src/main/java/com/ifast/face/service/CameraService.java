package com.ifast.face.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ifast.common.utils.Result;
import com.ifast.face.domain.Camera;


public interface CameraService extends IService<Camera>{
	public Result<?> push(String id,String userId); 
	public Result<?> endPush(String id); 
	public List<Camera> getMapCameraByFourCorners(String minX,String minY,String maxX,String maxY);

}
