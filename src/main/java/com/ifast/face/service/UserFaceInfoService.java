package com.ifast.face.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ifast.common.utils.Result;
import com.ifast.face.domain.UserFaceInfo;


public interface UserFaceInfoService extends IService<UserFaceInfo>{

    UserFaceInfo saveFace(UserFaceInfo userFaceInfo);
    
    
    List<UserFaceInfo> findByFaceIds(List<String> groupId);
    
    void deleteInBatch(List<UserFaceInfo> users);
    
    IPage<UserFaceInfo> selectPage(Page<UserFaceInfo> page);
    
    IPage<UserFaceInfo> selectPage(Page<UserFaceInfo> page, Wrapper<UserFaceInfo> wrapper);
    
    /**新增*/
	public Result<?> add(HttpServletRequest request,UserFaceInfo news) throws Exception;
	/**新增*/
	public Result<?> update(HttpServletRequest request,UserFaceInfo news) throws Exception;
}
