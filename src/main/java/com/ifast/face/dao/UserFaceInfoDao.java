package com.ifast.face.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.ifast.common.base.BaseDao;
import com.ifast.face.domain.UserFaceInfo;

@Repository
public interface UserFaceInfoDao extends BaseDao<UserFaceInfo>{
	@Select("<script>"
			+"SELECT id,name,face_id as faceId,face_feature as faceFeature,group_id as groupId FROM USER_FACE_INFO WHERE GROUP_ID = #{groupId} and checked = 0"
			+"</script>")
    List<UserFaceInfo> findByGroupId(Long groupId);
	@Select("<script>"
			+"SELECT id, name,age,gender,face_id as faceId,IMG_PATH as imgPath,group_id as groupId FROM USER_FACE_INFO WHERE "
			+ "FACE_ID in <foreach item='faceId' collection='faceIds' open='(' separator=',' close=')' > #{faceId} </foreach> " 
			+"</script>")
    List<UserFaceInfo> findByFaceId(@Param("faceIds")List<String> faceIds);
}
