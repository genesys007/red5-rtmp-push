package com.ifast.common.dao;

import com.ifast.common.base.BaseDao;
import com.ifast.common.domain.Update;

/**
 * @ClassName UpdateDao
 * @Author ShiQiang
 * @Date 2019年4月24日10:40:14
 * @Version v1.0
 */
public interface UpdateDao extends BaseDao<Update> {}
