package com.ifast.common.mybatiscomm;

import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.ifast.common.utils.ShiroUtils;
import com.ifast.sys.domain.UserDO;
import com.xiaoleilu.hutool.date.DateUtil;

/**
 * @ClassName MyMetaObjectHandler
 * @Author hailong.zheng
 * @Date 2018/8/29 19:34
 * @Version v1.0
 */
public class MyMetaObjectHandler implements MetaObjectHandler {

    protected final static Logger logger = LoggerFactory.getLogger(MyMetaObjectHandler.class);

    @Override
    public void insertFill(MetaObject metaObject) {
    	try {
    		UserDO currUser = ShiroUtils.getSysUser();
        	if(currUser != null){
        		setFieldValByName("createBy", currUser.getId()+"", metaObject);
        		setFieldValByName("createId", currUser.getId()+"", metaObject);
        	}
            logger.info("添加createtime");
            Object createTime = getFieldValByName("createTime", metaObject);
            if (createTime == null) {
                setFieldValByName("createTime", DateUtil.now(), metaObject);
            }
		} catch (Exception e) {
			logger.info("默认添加方法异常"+e.getMessage()); 
		}
    	
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        try {
        	logger.info("添加updateTime");
            UserDO currUser = ShiroUtils.getSysUser();
        	if(currUser != null){
        		setFieldValByName("updateBy", currUser.getId()+"", metaObject);
        		setFieldValByName("updateId", currUser.getId()+"", metaObject);
        	}
            Object updateDate = getFieldValByName("updateTime", metaObject);
            if (null == updateDate) {
                setFieldValByName("updateTime", DateUtil.now(), metaObject);
            }
		} catch (Exception e) {
			logger.info("默认添加方法异常"+e.getMessage()); 
		} 
    } 
}
