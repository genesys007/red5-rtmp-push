package com.ifast.common.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;

import com.ifast.common.annotation.Log;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.domain.DictDO;
import com.ifast.common.service.DictService;
import com.ifast.common.utils.Result;
import com.xiaoleilu.hutool.util.CollectionUtil;
import com.xiaoleilu.hutool.util.StrUtil;

/**
 * <pre>
 * 数据字典
 * </pre>
 * 
 * <small> 2018年3月23日 | Aron</small>
 */
@Controller
@RequestMapping("/common/sysDict")
public class DictController extends AdminBaseController {
    
	@Autowired
    private DictService sysDictService;
	
    @GetMapping()
    @RequiresPermissions("common:sysDict:sysDict")
    String sysDict() {
        return "common/sysDict/sysDict";
    }
    
    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("common:sysDict:sysDict")
    public Result<IPage<DictDO>> list(DictDO dictDTO) {
    	QueryWrapper<DictDO> wrapper = new QueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(dictDTO.getName()),"name",dictDTO.getName());
        wrapper.like(StrUtil.isNotBlank(dictDTO.getType()),"type",dictDTO.getType());
        //wrapper.eq("del_flag",Const.DelEnum.no.getValue());
        wrapper.orderByAsc("type");
        wrapper.orderByAsc("create_date");
        IPage<DictDO> page = sysDictService.page(getPage(DictDO.class),wrapper);
        return Result.ok(page);
    }
    
    @GetMapping("/add")
    @RequiresPermissions("common:sysDict:add")
    String add() {
        return "common/sysDict/add";
    }

    @GetMapping("/edit/{id}")
    @RequiresPermissions("common:sysDict:edit")
    String edit(@PathVariable("id") String id, Model model) {
        DictDO sysDict = sysDictService.getById(id);
        model.addAttribute("sysDict", sysDict);
        return "common/sysDict/edit";
    }

    /**
     * 保存
     */
    @Log("添加数据字典")
    @ResponseBody
    @PostMapping("/save")
    @RequiresPermissions("common:sysDict:add")
    public Result<String> save(DictDO sysDict) {
        sysDictService.save(sysDict);
        return Result.ok();
    }

    /**
     * 修改
     */
    @Log("更新数据字典")
    @ResponseBody
    @RequestMapping("/update")
    @RequiresPermissions("common:sysDict:edit")
    public Result<String> update(DictDO sysDict) {
        sysDictService.updateById(sysDict);
        return Result.ok();
    }

    /**
     * 删除
     */
    @Log("删除数据字典")
    @PostMapping("/remove")
    @ResponseBody
    @RequiresPermissions("common:sysDict:remove")
    public Result<String> remove(String id) {
    	QueryWrapper<DictDO> wrapper = new QueryWrapper<DictDO>();
        wrapper.eq("parentId",id);
        List<DictDO> dictDOList = this.sysDictService.list(wrapper);
        if(CollectionUtil.isNotEmpty(dictDOList)){
            Result<String> result = new Result<>();
            result.setMsg(Result.NO_DEL);
            result.setCode(Result.CODE_FAIL);
            return result;
        }
        DictDO dictDO =  this.sysDictService.getById(id);
       // dictDO.setDelFlag(Const.DelEnum.yes.getValue());
        sysDictService.updateById(dictDO);
        return Result.ok();
    }

    /**
     * 删除
     */
    @Log("删除数据字典")
    @PostMapping("/batchRemove")
    @ResponseBody
    @RequiresPermissions("common:sysDict:batchRemove")
    public Result<String> remove(@RequestParam("ids[]") String[] ids) {
        sysDictService.removeByIds(Arrays.asList(ids));
        return Result.ok();
    }
    
    @GetMapping("/type")
    @ResponseBody
    public List<DictDO> listType() {
        return sysDictService.listType();
    };

    @GetMapping("/typeListByJCLB")
    @ResponseBody
    public List<DictDO> typeListByJCLB() {
        return sysDictService.typeListByJCLB();
    };

    // 类别已经指定增加
    @GetMapping("/add/{type}/{parentId}")
    @RequiresPermissions("common:sysDict:add")
    String addD(Model model, @PathVariable("type") String type,@PathVariable("parentId") String parentId) {
        model.addAttribute("type", type);
        model.addAttribute("parentId", parentId);
        return "common/sysDict/add";
    }
    
    @ResponseBody
    @GetMapping("/list/{type}")
    public List<DictDO> listByType(@PathVariable("type") String type) {
        // 查询列表数据
        Map<String, Object> map = new HashMap<>(16);
        map.put("type", type);
        List<DictDO> dictList = (List<DictDO>) sysDictService.listByMap(map);
        return dictList;
    }

    @ResponseBody
    @GetMapping("/listByGroup")
    public Result<List<DictDO>> listByGroup(String gValue) { 
        return Result.ok(sysDictService.list(new QueryWrapper<DictDO>().eq("PARENTID",gValue).eq("DEL_FLAG", "").orderByDesc("sort")));
    }
    
    @ResponseBody
    @GetMapping("/listByGroupType")
    public Result<List<DictDO>> listByGroupO(String gValue) { 
        return Result.ok(sysDictService.list( new QueryWrapper<DictDO>().eq("type",gValue).orderByAsc("sort")));
    }
    
    @GetMapping("/getExamType")
    @ResponseBody
    public Result<?> getExamType(){
    	QueryWrapper<DictDO> wrapperDict = new QueryWrapper<>();
        wrapperDict.select("name,value")
               // .eq("PARENTID",Const.examTmfl.fzz.getId())
                .isNotNull("value")
                .orderByDesc("sort");
        List<DictDO> listDictDO = this.sysDictService.list(wrapperDict);
        return Result.ok(listDictDO);
    }
    
}
